package test;

import com.supermarket.offer.item.*;
import com.supermarket.offer.rule.service.RuleManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class RuleManagerTest
{

    RuleManager ruleManager;
    Basket basket ;
    HashMap<Item, Integer> itemQuantity;
    Item appelItem;
    Item orangeItem;
    Item watermelonItem;

    @Before
    public void setUp() {
        ruleManager = new RuleManager();
        appelItem = new AppleItem();
        appelItem.setPrice(0.20f);
        orangeItem = new OrangeItem();
        orangeItem.setPrice(0.50f);
        watermelonItem = new WatermelonItem();
        watermelonItem.setPrice(0.80f);
        basket = new Basket();
       itemQuantity = new HashMap<>();
    }

    @Test
    public void testOnlyAppleRule() {
        itemQuantity.put(appelItem, 2);
        itemQuantity.put(orangeItem, 0);
        itemQuantity.put(watermelonItem,0);
        basket.setItemQuantity(itemQuantity);
       Assert.assertEquals(ruleManager.calculate(basket), 0.2f, 0);

    }


    @Test
    public void testOnlyWatermelonRule() {
        itemQuantity.put(appelItem, 0);
        itemQuantity.put(orangeItem, 0);
        itemQuantity.put(watermelonItem, 3);
        basket.setItemQuantity(itemQuantity);
        Assert.assertEquals(ruleManager.calculate(basket), 1.6f, 0);

    }


    @Test
    public void testOnlyOrangeRule() {
        itemQuantity.put(appelItem, 0);
        itemQuantity.put(orangeItem, 2);
        itemQuantity.put(watermelonItem, 0);
        basket.setItemQuantity(itemQuantity);
        Assert.assertEquals(ruleManager.calculate(basket), 1.0f, 0);

    }


    @Test
    public void testAllRules() {
        itemQuantity.put(appelItem, 2);
        itemQuantity.put(orangeItem, 2);
        itemQuantity.put(watermelonItem, 3);
        basket.setItemQuantity(itemQuantity);
        Assert.assertEquals(ruleManager.calculate(basket), 2.8f, 0);

    }


    @Test
    public void testAllRules1() {
        itemQuantity.put(appelItem,4);
        itemQuantity.put(orangeItem, 3);
        itemQuantity.put(watermelonItem, 5);
        basket.setItemQuantity(itemQuantity);
        Assert.assertEquals(ruleManager.calculate(basket), 5.1f, 0);

    }


}