package com.supermarket.offer.rule;

import com.supermarket.offer.item.AppleItem;
import com.supermarket.offer.item.Item;

public class AppleOfferRule implements Rule<AppleItem> {


    public AppleOfferRule(Item item) {
    }

    @Override
    public float compute(AppleItem item, float quantity) {

        return ((int) quantity / 2 + quantity % 2) * item.getPrice();

    }
}
