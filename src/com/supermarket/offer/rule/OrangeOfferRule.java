package com.supermarket.offer.rule;

import com.supermarket.offer.item.Item;
import com.supermarket.offer.item.OrangeItem;

public class OrangeOfferRule implements Rule<OrangeItem> {


    public OrangeOfferRule(Item item) {
    }

    @Override
    public float compute(OrangeItem item, float quantity) {

        return quantity * item.getPrice();
    }
}
