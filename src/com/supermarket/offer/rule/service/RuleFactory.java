package com.supermarket.offer.rule.service;

import com.supermarket.offer.item.Item;
import com.supermarket.offer.item.WatermelonItem;
import com.supermarket.offer.item.AppleItem;
import com.supermarket.offer.item.OrangeItem;
import com.supermarket.offer.rule.AppleOfferRule;
import com.supermarket.offer.rule.OrangeOfferRule;
import com.supermarket.offer.rule.Rule;
import com.supermarket.offer.rule.WatermelonsOfferRule;

public class RuleFactory {

    public static Rule getRule(Item item) throws Exception {
        if (item instanceof AppleItem) {
            return new AppleOfferRule(item);
        } else if (item instanceof OrangeItem) {
            return new OrangeOfferRule(item);
        } else if (item instanceof WatermelonItem) {
            return new WatermelonsOfferRule(item);
        } else {
            throw new Exception("No offer found ");
        }

    }
}
