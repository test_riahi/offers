package com.supermarket.offer.rule.service;

import com.supermarket.offer.item.Item;
import com.supermarket.offer.item.Basket;
import com.supermarket.offer.rule.Rule;

public class RuleManager {

    public float calculate(Basket basket) {
        float total = 0f;
        try {
            if (basket != null && !basket.getItemQuantity().isEmpty()) {

                for (Item item : basket.getItemQuantity().keySet()) {

                    Rule rule = RuleFactory.getRule(item);
                    total += rule.compute(item, basket.getItemQuantity().get(item));

                }
                System.out.println("Total price: " + total);

            }
        } catch (Exception e) {
            System.console().printf("Cannot compute tola", e.getMessage());
        }
        return total;
    }
}
