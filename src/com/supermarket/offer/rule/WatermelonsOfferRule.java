package com.supermarket.offer.rule;


import com.supermarket.offer.item.Item;
import com.supermarket.offer.item.WatermelonItem;

public class WatermelonsOfferRule implements Rule<WatermelonItem> {


    public WatermelonsOfferRule(Item item) {

    }

    @Override
    public float compute(WatermelonItem item, float quantity) {

        return (2 * (int) (quantity / 3) + quantity % 3) * item.getPrice();

    }


}
