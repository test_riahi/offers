package com.supermarket.offer.rule;

public interface Rule<Item> {


    public float compute(Item item, float quantity);
}
