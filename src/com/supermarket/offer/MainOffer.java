package com.supermarket.offer;

import com.supermarket.offer.item.*;
import com.supermarket.offer.rule.service.RuleManager;

import java.util.HashMap;

public class MainOffer {


    public static void main(String[] args) {

        Item appelItem = new AppleItem();
        appelItem.setPrice(0.20f);
        Item orangeItem = new OrangeItem();
        orangeItem.setPrice(0.50f);
        Item watermelonItem = new WatermelonItem();
        watermelonItem.setPrice(0.80f);
        Basket basket = new Basket();


        HashMap<Item, Integer> itemQuantity = new HashMap<>();
        itemQuantity.put(appelItem,Integer.valueOf(args[0]));
        itemQuantity.put(orangeItem, Integer.valueOf(args[1]));
        itemQuantity.put(watermelonItem, Integer.valueOf(args[2]));
        basket.setItemQuantity(itemQuantity);


        RuleManager ruleManager = new RuleManager();
        ruleManager.calculate(basket);


    }
}
