package com.supermarket.offer.item;

public abstract class Item {


    private float price;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


}
