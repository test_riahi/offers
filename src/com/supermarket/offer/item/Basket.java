package com.supermarket.offer.item;

import java.util.HashMap;

public class Basket {


    private HashMap<Item, Integer> itemQuantity;


    public HashMap<Item, Integer> getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(HashMap<Item, Integer> itemQuantity) {
        this.itemQuantity = itemQuantity;
    }


}
